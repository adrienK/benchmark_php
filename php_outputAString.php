<?php
$string = 'je suis un "string" ...';
$int    = 42;

function print_B()
{
    global $string, $int;

    print $string.$int;
    print 'Blablabla M.Freeman';
    print 'Blablabla M.Freeman'.$string.$int;
}

function echo_B()
{
    global $string, $int;

    echo $string.$int;
    echo 'Blablabla M.Freeman';
    echo 'Blablabla M.Freeman'.$string.$int;
}

function sprintf_B()
{
    global $string, $int;

    sprintf("%s%d", $string, $int);
    sprintf("Blablabla M.Freeman");
    sprintf("Blablabla M.Freeman %s%d", $string, $int);
}

function printf_B()
{
    global $string, $int;

    printf("%s%d", $string, $int);
    printf("Blablabla M.Freeman");
    printf("Blablabla M.Freeman %s%d", $string, $int);
}

for ($i = 0; $i < 100000; $i++) {
    print_B();
    echo_B();
    sprintf_B();
    printf_B();
}
