<?php
function null_B()
{
        $a = 'athos porthos aramis';
        $b = array('athos' => 'Milady de Winter', 'Haaa les femmes ...');

        $a          = NULL;
        $b['athos'] = NULL;
        $b['0']     = NULL;
}

function unset_B()
{
        $a = 'athos porthos aramis';
        $b = array('athos' => 'Milady de Winter', 'Haaa les femmes ...');

        unset($a);
        unset($b['athos']);
        unset($b['0']);
}

for ($i = 0; $i < 100000; $i++) {
    null_B();
    unset_B();
}
