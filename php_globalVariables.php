<?php
// Pour plus d'équité variables_def() et staticVariables_def() sont appelé 10 fois comme php::define
// En utilisant des variables statiques dans une fonction il est aussi possible de les redeffinires
// Le defaut des fonctions de variables est qu'il faut optimiser les appeles comme avec une BDD sinon on sur charge

function constants_def()
{
    define("hall0000", "f<e");
    define("hall1000", "yrk");
    define("hall2000", "sj,");
    define("hall3000", "zrtzyj");
    define("hall4000", "rzthnj");
    define("hall5000", "ioù");
    define("hall6000", "att");
    define("hall7000", "4tb2");
    define("hall8000", "zzrtny2");
    define("hall9000", ",ty");
}

function variables_def($arg)
{
    $hall0000 = "zrtb";
    $hall1000 = "zrtny2";
    $hall2000 = "4zrty";
    $hall3000 = "4!ç=$";
    $hall4000 = "ynzi2";
    $hall5000 = "!=ç";
    $hall6000 = "zl";
    $hall7000 = "tlzyyyy";
    $hall8000 = "zlyt";
    $hall9000 = "zlryurtj";

    return ${$arg};
}

function staticVariables_def($arg)
{
    static $hall0000 = "synuu";
    static $hall1000 = "op:t";
    static $hall2000 = "zrty;";
    static $hall3000 = ";zrty";
    static $hall4000 = "42ld";
    static $hall5000 = "4;tpm,2";
    static $hall6000 = "qeyryur";
    static $hall7000 = "(rsy,)";
    static $hall8000 = "4ç-;e";
    static $hall9000 = ",-r";

    return ${$arg};
}

function constants_used()
{
    constants_def();

    $a = hall5000;
}

function variables_used()
{
    for ($i = 0; $i < 10; ++$i) {
        $a = variables_def("hall".$i."000");
        echo "string";
    }
}

function staticVariables_used()
{
    for ($i = 0; $i < 10; ++$i) {
        $a = staticVariables_def("hall".$i."000");
    }
}

constants_used();
variables_used();
staticVariables_used();
