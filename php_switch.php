<?php
function if_A()
{
        if (false) {}
        elseif (false) {}
        else {}
}

function if_B()
{
        if (false) {}
        elseif (false) {}
        elseif (false) {}
        elseif (false) {}
        elseif (false) {}
        else {}
}

function if_C()
{
        if (false) {}
        if (false) {}
        if (false) {}
        if (false) {}
        if (false) {}
        else {}
}

function switch_A()
{
        switch (true) {
            case false: break;
            case false: break;
            default:  break;
        }
}

function switch_B()
{
        switch (true) {
            case false: break;
            case false: break;
            case false: break;
            case false: break;
            case false: break;
            default:  break;
        }
}

for ($i = 0; $i < 100000; $i++) {
    if_A();
    if_B();
    if_C();
    switch_A();
    switch_B();
}
