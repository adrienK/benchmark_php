<?php
$str = 'Fallout > 3, is not a Fallout game!';
$int = 42;

function strcEgal()
{
    global $str, $int;

        if ('Fallout > 3, is not a Fallout game!' === $str);
        if ('The cake is a lie'                   === $str);

        if (42 === $int);
        if (24 === $int);
        if ('24' === $int);
}

function egal()
{
    global $str, $int;

        if ('Fallout > 3, is not a Fallout game!' == $str);
        if ('The cake is a lie'                   == $str);

        if (42 == $int);
        if (24 == $int);
        if ('24' == $int);
}

function strcdiff()
{
    global $str, $int;

        if ('Fallout > 3, is not a Fallout game!' !== $str);
        if ('The cake is a lie'                   !== $str);

        if (42 !== $int);
        if (24 !== $int);
        if ('24' !== $int);
}

function diff()
{
    global $str, $int;

        if ('Fallout > 3, is not a Fallout game!' != $str);
        if ('The cake is a lie'                   != $str);

        if (42 != $int);
        if (24 != $int);
        if ('24' != $int);
}

function strcEgal_cast()
{
    global $str, $int;

        if ((string) 'Fallout > 3, is not a Fallout game!' === $str);
        if ((string) 'The cake is a lie'                   === $str);

        if ((int) 42 === $int);
        if ((int) 24 === $int);
        if ((string) '24' === $int);
}

function egal_cast()
{
    global $str, $int;

        if ((string) 'Fallout > 3, is not a Fallout game!' == $str);
        if ((string) 'The cake is a lie'                   == $str);

        if ((int) 42 == $int);
        if ((int) 24 == $int);
        if ((string) '24' == $int);
}

function strcdiff_cast()
{
    global $str, $int;

        if ((string) 'Fallout > 3, is not a Fallout game!' !== $str);
        if ((string) 'The cake is a lie'                   !== $str);

        if ((int) 42 !== $int);
        if ((int) 24 !== $int);
        if ((string) '24' !== $int);
}

function diff_cast()
{
    global $str, $int;

        if ((string) 'Fallout > 3, is not a Fallout game!' != $str);
        if ((string) 'The cake is a lie'                   != $str);

        if ((int) 42 != $int);
        if ((int) 24 != $int);
        if ((string) '24' != $int);
}

for ($i = 0; $i < 100000; $i++) {
    strcEgal();
    egal();
    strcEgal_cast();
    egal_cast();

    strcdiff();
    diff();
    strcdiff_cast();
    diff_cast();
}
