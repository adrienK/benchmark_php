<?php
$array = array('42' => 'The Truth Is Out There', 'json' => 'une case en moins', 'toujours là');

function foreach_kv()
{
    global $array;

    foreach ($array as $key => $value);
}

function foreach_kPv()
{
    global $array;

    foreach ($array as $key => &$value);
}

function foreach_v()
{
    global $array;

    foreach ($array as $value);
}

function while_k()
{
    global $array;

    while (list($key) = each($array));
}

function for_k()
{
    global $array;

    $key  = array_keys($array);
    $size = count($key);

    for ($i=0; $i < $size; $i++);
}

for ($i = 0; $i < 100000; $i++) {
    foreach_kv();
    foreach_kPv();
    foreach_v();
    while_k();
    for_k();
}
