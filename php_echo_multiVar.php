<?php
$string = 'je suis un "string" ...';
$int    = 42;

function echo_gui_var()
{
    global $string, $int;

    echo "$string$int$string$int$string$int$string$int";
}

function echo_apo_var_dot()
{
    global $string, $int;

    echo $string.$int.$string.$int.$string.$int.$string.$int;
}

function echo_apo_var_com()
{
    global $string, $int;

    echo $string,$int,$string,$int,$string,$int,$string,$int;
}

for ($i = 0; $i < 100000; $i++) {
    echo_gui_var();
    echo_apo_var_dot();
    echo_apo_var_com();
}
