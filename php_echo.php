<?php
// Global est est utilisé dans toute les fonctions car il serait compté comme temps suplémentaire

$string = 'je suis un "string" ...';
$int    = 42;

function echo_gui()
{
    global $string, $int;

    echo "je suis une chaine, bastard !";
}

function echo_apo()
{
    global $string, $int;

    echo 'je suis une chaine, bastard !';
}

function echo_gui_var()
{
    global $string, $int;

    echo "je suis une chaine, bastard ! $string de $int";
}

function echo_apo_var_dot()
{
    global $string, $int;

    echo 'je suis une chaine, bastard !'.$string.' de '.$int;
}

function echo_apo_var_com()
{
    global $string, $int;

    echo 'je suis une chaine, bastard !',$string,' de ',$int;
}

for ($i = 0; $i < 100000; $i++) {
    echo_gui();
    echo_apo();
    echo_gui_var();
    echo_apo_var_dot();
    echo_apo_var_com();
}
